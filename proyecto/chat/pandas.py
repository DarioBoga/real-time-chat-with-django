import pandas as pd
import sys
import matplotlib.pyplot as plt 
import seaborn as sns

                                              
import os 
dir_path = os.path.dirname(os.path.realpath(__file__))

color = sns.color_palette()

#Imprime las 10 canciones más escuchadas del top
def top10():
    fichero = dir_path +'/datos19.csv'
    df = pd.read_csv(fichero, usecols= ['Track.Name','Artist.Name'],encoding='ISO-8859-1')
    listadf = df.head(10)
    lista = listadf.to_string()
    return lista
    

#Calculamos qué genero está más veces en el top50
def Genre():
    
    fichero = dir_path +'/datos19.csv'
    df = pd.read_csv(fichero,encoding='ISO-8859-1')

    grouped_data = df.groupby('Genre')
    count_songs_by_genre = grouped_data.count()['Track.Name']
    ordenados = count_songs_by_genre.sort_values()
    lista  = ordenados.to_string()
    count_songs_by_genre.plot(kind='bar', figsize=(15,12))
    plt.savefig('genre.png')
    return lista

#Ordenamos canciones por popularidad y mostramos la lista.
def Popularity():
     
    fichero = dir_path +'/datos19.csv'
    df = pd.read_csv(fichero, usecols=['Track.Name','Artist.Name','Popularity'],encoding='ISO-8859-1')
    df = df.sort_values('Popularity', ascending=False)
    lista  = df.to_string()
    return lista

#Calculamos cual es el género musical con un indice de popularidad más alto
def PopularityGenre():
     
    fichero = dir_path +'/datos19.csv'
    df = pd.read_csv(fichero,encoding='ISO-8859-1')

    grouped_data = df.groupby('Genre')
    count_songs_by_genre = grouped_data.sum()['Popularity']
    lista  = count_songs_by_genre.to_string()
    count_songs_by_genre.plot(kind='bar', figsize=(15,12))
    plt.savefig('popularidad.png')
    return lista

#Calculamos el número de canciones por artista en el top50

def NumSongs():
    fichero = dir_path +'/datos19.csv'
    df = pd.read_csv(fichero,encoding='ISO-8859-1')

    grouped_data = df.groupby('Artist.Name')
    count_songs_by_genre = grouped_data.count()['Track.Name']
    lista  = count_songs_by_genre.to_string()
    count_songs_by_genre.plot(kind='barh', figsize=(15,12))
    plt.savefig('songs.png')
    return lista


#Relación entre bps y popularidad
def BpsPop():
    fichero = dir_path + '/datos19.csv'
    df = pd.read_csv(fichero, usecols=['Track.Name','Beats.Per.Minute','Popularity'],encoding='ISO-8859-1')

    lista  = df.to_string()
    plt.figure(figsize=(15,15))
    sns.jointplot(x=df["Beats.Per.Minute"].values, y=df['Popularity'].values, size=12, kind="reg",color=color[7])
    plt.ylabel('Popularity', fontsize=11)
    plt.xlabel("Bpm", fontsize=11)
    plt.title("Bpm Vs Popularity", fontsize=15)
    plt.savefig('beas&popularity.png')
    
    return lista


#Las canciones populares más bailables
def Danceability():
    fichero = dir_path + '/datos19.csv'
    df = pd.read_csv(fichero, usecols=['Track.Name','Danceability','Popularity'],encoding='ISO-8859-1')

    lista  = df.to_string()
    plt.figure(figsize=(15,15))
    sns.violinplot(x=df["Danceability"].values, y=df['Popularity'].values, size=12, kind="reg",color=color[3])
    plt.ylabel('Popularity', fontsize=11)
    plt.xlabel("Danceability", fontsize=11)
    plt.title("Danceability Vs Popularity", fontsize=15)
    plt.savefig('danceability.png')
    
    return lista