import json
from channels.generic.websocket import AsyncWebsocketConsumer
from chat.twitter import Tweet,getTweets,deleteTweets #Importamos la función para publicar el tweet (Creada en twitter.py)
from chat.telegram import Send_message
from chat.youtube import *
from chat.pandas import *

class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name


        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        
        username = self.scope["user"].username

        room_data=self.scope['url_route']['kwargs']['room_name'] #Cojo el nombre de la sala
        video = getVideoWatch() #Video que se está reproduciendo
        mensaje='Currently listening ' + video + ' . Come and join us in http://127.0.0.1:8000/chat/'+room_data

        if len(message)>1:
            message_str=str(message)    
            splitted = message_str.split()
            first = splitted[0]

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#   Para la API de twitter
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            if message[0]=='!':
                if first.lower() == '!tweet':
                    if len(message)>6:
                        await self.channel_layer.group_send(
                            self.room_group_name,
                            {
                                'type': 'chat_message',
                                'message': username+': ' + message + "\n"
                                            'MusicBot: Too much arguments. To post a tweet, use !tweet command without more arguments.'
                            }
                        )
                    else:
                        tweetsEnviados = getTweets()
                        enviar=True
                        for tweet in tweetsEnviados:
                            if(tweet==mensaje):
                                enviar=False
                        if (enviar):
                            Tweet(mensaje) #Llamo a la función Tweet, declarada en el archivo twitter.py
                            await self.channel_layer.group_send(
                                self.room_group_name,
                                {
                                    'type': 'chat_message',
                                    'message': username+': ' + message + "\n"
                                                'MusicBot: TweetPosted'
                                }
                            )
                        else:
                            await self.channel_layer.group_send(
                                self.room_group_name,
                                {
                                    'type': 'chat_message',
                                    'message': username+': ' + message + "\n"
                                                'MusicBot: Cannot send tweet. Tweet already posted.'
                                }
                            )

                elif first.lower() == '!deletetweets':
                    
                    if len(message)>13:
                        await self.channel_layer.group_send(
                            self.room_group_name,
                            {
                                'type': 'chat_message',
                                'message': username+': ' + message + "\n"
                                            'MusicBot: Too much arguments. To delete tweets, use !deletetweets command without more arguments.'
                            }
                        )
                    else:
                        deleteTweets(room_data)
                        await self.channel_layer.group_send(
                                self.room_group_name,
                                {
                                    'type': 'chat_message',
                                    'message': username+': ' + message + "\n"
                                                'MusicBot: Tweets from room ' + room_data + ' deleted.'
                                }
                            )

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#   Para la API de Telegram
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                elif first.lower() == '!telegram':
                    if len(message)>9:
                        await self.channel_layer.group_send(
                            self.room_group_name,
                            {
                                'type': 'chat_message',
                                'message': username+': ' + message + "\n"
                                            'MusicBot: Too much arguments. To share on telegram channel, use !telegram command without more arguments.'
                            }
                        )
                    else:
                        Send_message(mensaje)
                        await self.channel_layer.group_send(
                                self.room_group_name,
                                {
                                    'type': 'chat_message',
                                    'message': username+': ' + message + "\n"
                                                'MusicBot: Message sent to the Telegram channel.'
                                }
                            )

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#   Para la API de Youtube
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                elif first.lower() == '!search':
                    separador = '!search'
                    palabras_clave = message.split(separador)
                    Search(palabras_clave)
                   
                    enlace = getVideo() #Enviamos al Search las palabras claves para buscar el video 
                    await self.channel_layer.group_send(
                        self.room_group_name,
                        {
                            'type': 'chat_message',
                            'message': username+': ' + "\n"+ enlace
                        }
                    )
                elif first.lower() == '!play':
                    
                    await self.channel_layer.group_send(
                        self.room_group_name,
                        {
                            'type': 'chat_message',
                            'message': username+': ' + message
                        }
                    )

                elif first.lower() == '!stop':
                    if len(message)>5:
                        await self.channel_layer.group_send(
                            self.room_group_name,
                            {
                                'type': 'chat_message',
                                'message': username+': ' + message + "\n"
                                            'MusicBot: Too much arguments. To stop the video, use !stop command without more arguments.'
                            }
                        )
                    else:
                        await self.channel_layer.group_send(
                            self.room_group_name,
                            {
                                'type': 'chat_message',
                                'message': username+': ' + message 
                            }
                        )

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#   Para pedir información con Pandas
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                elif first.lower() == '!info':
                    separador = '!info'
                    clave = message.split(separador)[1]
                    info = ''
                    if clave == ' genre':
                        info = Genre() 
                    elif clave == ' songs':
                        info = NumSongs()
                    elif clave == ' popularity': 
                        info = Popularity()
                    elif clave == ' bps':
                        info = BpsPop()
                    elif clave == ' popularitygenre':
                        info = PopularityGenre()
                    elif clave == ' danceability':
                        info = Danceability()
                    elif clave == ' top10':
                        info = top10()
                    else:
                        info = 'MusicBot: Invalid argument. Please, check the options in !help'
                     
                    await self.channel_layer.group_send(
                        self.room_group_name,
                        {
                            'type': 'chat_message',
                            'message': username+': ' + info 
                            
                        }
                    )

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#   Para información sobre los comandos
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
                elif first.lower() == '!help':
                    if len(message)>5:
                        await self.channel_layer.group_send(
                            self.room_group_name,
                            {
                                'type': 'chat_message',
                                'message': username+': ' + message + "\n"
                                            'MusicBot: Too much arguments. To see commands available, use !help command without more arguments.'
                            }
                        )
                    else:
                        await self.channel_layer.group_send(
                            self.room_group_name,
                            {
                                'type': 'chat_message',
                                'message':  username+': ' + message + "\n"
                                            "MusicBot: The commands available are:\n" 
                                            "   !search video_to_search: Searches the video in youtube and puts it in the video player \n"
                                            "   !play: Shows the video player \n"
                                            "   !stop: Hides the video player \n"
                                            "   !tweet: Tweets the current video and link to the room in the ChannelsMusic official twitter \n"
                                            "   !deleteTweets: Delete all the tweets sent from the room \n"
                                            "   !telegram: Sends the current video and link to the room to the official ChanneldMusic telegram channel \n"
                                            "   !info (genre/popularity/songs/danceability/popularitygenre/top10) to see information about the most listened songs"
                            }
                        )

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#   Si es un comando erróneo (el string empieza por ! pero no es ninguno de la lista)
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
                else: 
                    await self.channel_layer.group_send(
                        self.room_group_name,
                        {
                            'type': 'chat_message',
                            'message': username+': ' + message + "\n"
                                        'MusicBot: Command not recognized. Please, type !help to see the list of commands available'
                        }
                    )

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#   Para mensajes de chat normales
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
            else:   #Si no es un comando, es un mensaje de chat normal
            # Send message to room group
                await self.channel_layer.group_send(
                    self.room_group_name,
                    {
                        'type': 'chat_message',
                        'message': username+': ' + message 
                    }
                )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']

         # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message     
        }))