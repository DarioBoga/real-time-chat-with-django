"""
Utilizaremos ASGI_REDIS para almacenar la información de los mensajes. Por este motivo, estos no serán
persistentes (cuando cerremos la aplicación, los mensajes anteriores se borrarán, pero es
una forma sencilla de guardar temporalmente los mensajes del chat, sin tener que hacer uso
de bases de datos u otros recursos.)
"""

import os
import django
from channels.routing import get_default_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "multichat.settings")
django.setup()
application = get_default_application()
