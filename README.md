Nombre proyecto: Django Music Bot
-----------------------------------------------


Anotaciones previas:
---------------------------------------------------------------------------------------

Para el análisis de datos con pandas hemos optado por un archivo .csv que recopila el top50 canciones de 2019 de spotify, ya que nos proporcionaba información más completa e 
interesante a la hora de agrupar y generar gráficas. Hemos intentado sacar toda esta información trabajando con el API de Spotify pero no obteniamos toda la información que nos gustaría.
Las gráficas que son generadas cuando ejecutamos el comando "!info opción" no son las que se muestran en el welcome. Las gráficas que se muestyran han sido generadas anteriormente
para luego visualizarlas en la página Welcome.


Breve descripción de la aplicación, y un listado de las funcionalidades más relevantes:
---------------------------------------------------------------------------------------

La web consiste en un chat de peticiones musicales a youtube. Los usuarios conectados a una misma 
sala pueden enviar mensajes y además pedir canciones, que se buscarán en youtube y aparecerán en 
un reproductor al lado del chat.

Lo primero que vemos al entrar a la web es la pantalla de logeo. Aquí meteremos los datos de usuario (o crearemos una cuenta).

Una vez accedamos, nos llevará a la página de selección de sala. En el buscador, introduciremos un string (que representa el nombre de la sala)
y la web creará una sala con ese nombre, o bien podemos escoger una de las salas ya creadas, que simulan salas públicas. 
Estas salas consisten en un chat en tiempo real entre personas que se encuentran en la misma sala.

El chat funciona con comandos, entre los cuales se incluyen interacciones con distintas APIs. Estos son:


!search video_a_buscar: Busca el vídeo en youtube, coge el primer resultado y crea un reproductor con la url del vídeo. Cuando termina, recarga la página
			(se ha hecho así ya que el reproductor no se actualizaba con el nuevo vídeo hasta recargar la página, pero perderemos los mensajes
			de chat antiguos). 

!play -> Muestra el reproductor de vídeo (por defecto está en oculto) y redimensiona el tamaño del chat para que quepan ambas cosas (hecho con javascript).

!stop -> Esconde el reproductor de vídeo y redimensiona el tamaño del chat para que quepan ambas cosas (hecho con javascript).

!tweet -> Publica un tweet en la cuenta de twitter de la aplicación (https://twitter.com/ChannelsDjango). Este tweet contendrá la url
	  del vídeo que se está reproduciento en ese momento en la sala y la url completa a dicha sala de chat (para que así puedan entrar usuarios interesados).
	  Además, antes de publicar el tweet comprueba que ese tweet no haya sido publicado ya (la API de twitter da error si intentas publicar dos veces
	  un tweet con el mismo contenido).

!deleteTweets -> Borra todos los tweets de la cuenta de twitter publicados desde la sala actual (La API de twitter da error si intentas poner un tweet duplicado,
	       por lo que si los usuarios quisieran compartir dos veces el mismo vídeo desde la sala no podrían si no se les da la opción de borrar).

!telegram -> Manda el mismo mensaje que en el caso de tweet, pero en este caso en un canal de telegram.
	     Nota: Para este proyecto, el canal se ha hecho privado, aunque lo lógico es que en una aplicación real fuese público, pero haciéndolo privado
	           podemos controlar que no entre nadie más al canal.
	     El link para entrar al canal de telegram es: https://t.me/joinchat/AAAAAFae4cZhprvsWy1pvg

!info (genre/popularity/songs/danceability/popularitygenre/top10) to see information about the most listened songs" 
		Imprime por pantalla la información pedida con una de estas opciones

!help -> Muestra una lista de todos los comandos, su sintaxis y lo que hacen, para que los usuarios puedan consultarlo si lo necesitasen.


En el caso de introducir un comando de forma errónea se imprime un mensaje por pantalla recomendando el uso de !help.


Como ejecutar:
-----------------
Una vez clonado el repositorio, nos situaremos en él y ejecutaremos:
sh setup.sh


Problemas conocidos:
-------------------------------
No fuimos capaces de hacer que el reproductor de vídeo se actualizase con solo meter el comando !search. Para hacerlo funcional, hacemos que cada vez que
se usa el comando !search, se recargue automáticamente la página, quedando el vídeo buscado almacenado en el reproductor y ya se podría reproducir.
La desventaja de este método es que se pierden los mensajes del chat cuando recargas, pero puesto que la utilidad del chat es pedir canciones y escucharlas y no
chatear en sí, no consideramos los mensajes muy importantes.

Por falta de tiempo, no se ha podido implementar que casa sala de chat tenga su propio reproductor.

Si buscamos un video que contiene copyrigth no se reproduce (Problema del API de YouTube, no lo permite)
